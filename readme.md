
# Presentation for BAP

A presentation about __Business Application__ which is

* written as source code
* holds a build pipeline
* uses source code management

## To build it

To get it as __reveal js__ based presentation:

`make clean bap-presentation.html`

or as __pdf__ presentation:

`make clean bap-presentation.pdf`
